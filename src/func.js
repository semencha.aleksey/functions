const getSum = (str1, str2) => {

  if ( (typeof(str1) == 'string' && typeof(str2) == 'string') && !(isNaN(str1) && isNaN(str2)) )
  {
    return (Number(str1) + Number(str2)).toString();
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var authorPosts = 0;
  var authorComments = 0;
  for(const post of listOfPosts)
  {
    if (post['author'] == authorName) ++authorPosts;
    if ( typeof(post['comments']) !== 'undefined')
    {
      for(const comment of post['comments'])
      {
        if(comment['author'] == authorName) ++authorComments;
      }
    }
  }
  return 'Post:' + authorPosts + ',comments:' + authorComments;
};


const tickets=(people)=> {
  
  var ownMoney = 0;

  var fixedPrice = 25;

  for(const p of people)
  {
    if(p > fixedPrice && (p - fixedPrice) > ownMoney) return 'NO';

    ownMoney+=fixedPrice;
    ownMoney-=(p-fixedPrice);

  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
